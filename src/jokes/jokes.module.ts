import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { JokesService } from './jokes.service';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
  ],
  providers: [JokesService],
  exports: [JokesService],
})
export class JokesModule {}
