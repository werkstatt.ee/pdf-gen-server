import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { Injectable } from '@nestjs/common';

export interface Joke {
  categories: string[];
  createdAt: string;
  iconUrl: string;
  id: string;
  updatedAt: string;
  url: string;
  value: string;
}

@Injectable()
export class JokesService {
  constructor(private readonly httpService: HttpService) {}
  async loadJokes(jokeIds: string[]) {
    const requests: Promise<AxiosResponse<Joke, any>>[] = [];
    jokeIds.forEach((jokeId) => {
      requests.push(
        this.httpService
          .get<Joke>(`https://api.chucknorris.io/jokes/${jokeId}`)
          .toPromise(),
      );
    });

    return Promise.all(requests)
      .then((responses) => {
        return responses.map((response) => response.data);
      })
      .catch(() => {
        return [];
      });
  }
}
