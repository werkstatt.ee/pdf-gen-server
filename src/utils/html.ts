export function generateHtmlForJokes(texts: string[]) {
  return texts.reduce(
    (acc, text) => acc.concat(`<div class="joke">${text}</div>`),
    '',
  );
}
