import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DownloadsModule } from './downloads/downloads.module';
import { JokesModule } from './jokes/jokes.module';
import { EventsGateway } from './events/events.gateway';
import configuration from './config/configuration';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [configuration] }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('database.host'),
        port: +configService.get('database.port'),
        username: configService.get('database.user'),
        password: configService.get('database.password'),
        database: configService.get('database.name'),
        entities: ['dist/**/*.entity.{ts,js}'],
        autoLoadEntities: true,
        synchronize: true,
        cli: {
          migrationsDir: 'src/database/migrations',
        },
      }),
      inject: [ConfigService],
    }),
    DownloadsModule,
    JokesModule,
  ],
  controllers: [AppController],
  providers: [ConfigService, AppService, EventsGateway],
})
export class AppModule {}
