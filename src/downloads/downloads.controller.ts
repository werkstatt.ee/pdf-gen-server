import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Response,
  StreamableFile,
} from '@nestjs/common';
import * as fs from 'fs';
import { join } from 'path';
import { DownloadsService } from './downloads.service';
import { CreateDownloadDto } from './dto/create-download.dto';
import { UpdateDownloadDto } from './dto/update-download.dto';

@Controller('downloads')
export class DownloadsController {
  constructor(private readonly downloadsService: DownloadsService) {}

  @Post()
  create(@Body() createDownloadDto: CreateDownloadDto) {
    console.log('<---- generate download', createDownloadDto);
    return this.downloadsService.create(createDownloadDto);
  }

  @Get()
  findAll() {
    return this.downloadsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.downloadsService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDownloadDto: UpdateDownloadDto,
  ) {
    return this.downloadsService.update(+id, updateDownloadDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.downloadsService.remove(+id);
  }

  @Get('files/:filename')
  async openPdf(
    @Param('filename') filename: string,
    @Response({ passthrough: true }) res,
  ): Promise<StreamableFile> {
    try {
      const readableStream = fs.createReadStream(
        join(process.cwd(), `files/${filename}`),
      );

      res.set({
        'Content-Type': 'application/pdf',
        'Content-Disposition': `attachment; filename=${filename}`,
      });
      const streamdata = new StreamableFile(readableStream);
      return streamdata;
    } catch (error) {
      return error.message;
    }
  }
}
