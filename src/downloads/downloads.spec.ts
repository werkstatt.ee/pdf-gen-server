import { Test, TestingModule } from '@nestjs/testing';
import { Downloads } from './downloads';

describe('Downloads', () => {
  let provider: Downloads;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Downloads],
    }).compile();

    provider = module.get<Downloads>(Downloads);
  });

  it('should be defined', () => {
    expect(provider).toBeDefined();
  });
});
