import { Module } from '@nestjs/common';
import { DownloadsService } from './downloads.service';
import { DownloadsController } from './downloads.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Download } from './entities/download.entity';
import { EventsGateway } from 'src/events/events.gateway';
import { JokesModule } from 'src/jokes/jokes.module';

@Module({
  imports: [TypeOrmModule.forFeature([Download]), JokesModule],
  controllers: [DownloadsController],
  providers: [DownloadsService, EventsGateway],
})
export class DownloadsModule {}
