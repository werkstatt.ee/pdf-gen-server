import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Download extends BaseEntity {
  constructor(description: string) {
    super();
    this.description = description;
    this.isGenerated = false;
  }

  @PrimaryGeneratedColumn({ name: 'id' })
  id: number;

  @Column('text')
  description: string;

  @Column({
    default: null,
    nullable: true,
  })
  filename?: string;

  @Column()
  isGenerated: boolean;

  @Column({ name: 'updated_at', type: 'bigint', nullable: false })
  updatedAt: number;

  @Column({ name: 'created_at', type: 'bigint', nullable: false })
  createdAt: number;

  @BeforeInsert()
  @BeforeUpdate()
  public updateDates() {
    if (!this.createdAt) {
      this.createdAt = Math.floor(Date.now());
    }

    this.updatedAt = Math.floor(Date.now());
  }
}
