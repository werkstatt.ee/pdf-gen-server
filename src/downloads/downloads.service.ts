import { Injectable } from '@nestjs/common';
import { EventsGateway } from 'src/events/events.gateway';
import { CreateDownloadDto } from './dto/create-download.dto';
import { UpdateDownloadDto } from './dto/update-download.dto';
import { Download } from './entities/download.entity';

import * as fs from 'fs';
import { JokesService } from 'src/jokes/jokes.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { generateHtmlForJokes } from 'src/utils/html';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const pdf = require('html-pdf');

@Injectable()
export class DownloadsService {
  constructor(
    @InjectRepository(Download) private readonly repo: Repository<Download>,
    private readonly eventsGateWay: EventsGateway,
    private readonly jokesService: JokesService,
  ) {}
  async create(createDownloadDto: CreateDownloadDto) {
    if (createDownloadDto.jokeId.length === 0) {
      return;
    }

    const download = await new Download(
      createDownloadDto.jokeId.join('; '),
    ).save();
    if (!download || !download.id) {
      return;
    }
    this.loadAndCombineJokes(createDownloadDto.jokeId, download.id);
    return download;
  }

  findAll() {
    return this.repo.find();
  }

  findOne(id: number) {
    return this.repo.findOneBy({ id });
  }

  update(id: number, updateDownloadDto: UpdateDownloadDto) {
    console.log(updateDownloadDto);
    return `This action updates a #${id} download`;
  }

  remove(id: number) {
    return `This action removes a #${id} download`;
  }

  private async loadAndCombineJokes(jokeIds: string[], downloadId: number) {
    return new Promise(async (resolve, reject) => {
      const jokes = await this.jokesService.loadJokes(jokeIds);
      const texts = jokes.map((joke) => joke.value);
      const text = generateHtmlForJokes(texts);
      const filePath = `files/${downloadId}.pdf`;
      let html = await fs.promises.readFile('dist/assets/pdf.html', 'utf8');
      html = html.replace('{{jokes}}', text);
      const filesSuccess = await this.createPdf(html, filePath);
      if (!filesSuccess) {
        reject('Error saving file');
      }
      const update = await this.repo.update(
        { id: downloadId },
        { filename: filePath, isGenerated: true },
      );
      if (!update) {
        reject('Error updating entity');
      }
      const download = await this.findOne(downloadId);
      this.eventsGateWay.sendSuccess(download);
    });
  }

  private async createPdf(html: string, path: string): Promise<string> {
    return new Promise(async (resolve, reject) => {
      pdf.create(html, { format: 'Letter' }).toFile('./' + path, (err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res.filename);
      });
    });
  }
}
